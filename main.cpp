#include "stdafx.h"
#include "triangularMat.h"
#include "crsMatrix.h"


int main() {
	setlocale(LC_ALL, "Russian");
	
	cout << "---------------------------------" << endl;

	cout << "Тестирование треугольной матрицы:" << endl;
	cout << "Создаём матрицу 3х3" << endl;
	triangularMatrix A(3);
	A.print();
	cout << "Создаём вектор из 3 элементов:" << endl;
	vector<int> B = { 1, 2, 3 };
	for (int i = 0; i < 3; i++) cout << B[i] << " ";
	cout << endl;
	cout << "Умножаем матрицу на вектор:" << endl;
	A.MultVector(B);
	A.print();

	cout << "---------------------------------" << endl;
	cout << endl;
	cout << endl;
	cout << "---------------------------------" << endl;

	cout << "Тестирование разреженной матрицы:" << endl;
	cout << "Создаём матрицу 5х5 с 10 не нулевыми элементами:" << endl;
	crsMatrix С(5, 10);
	С.PrintValues();
	cout << "Создаём вектор из 10 элементов:" << endl;
	vector<int> D = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
	for (int i = 0; i < 10; i++) cout << D[i] << " ";
	cout << endl;
	cout << "Умножаем матрицу на вектор:" << endl;
	С.Multiplicate(D);
	С.PrintValues();

	cout << "---------------------------------" << endl;

	return 0;
}
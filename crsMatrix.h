#include <iostream>
#include <vector>
using namespace std;

class crsMatrix {

	int N; // Размер матрицы (N x N)
	int NZ; // Кол-во ненулевых элементов
	int* Value; // Массив значений (размер NZ)
	int* Col; // Массив номеров столбцов (размер NZ)
	int* RowIndex; // Массив индексов строк (размер N + 1)

public:
	crsMatrix(int _N, int _NZ) {
		N = _N;
		NZ = _NZ;
		Value = new int[NZ];
		Col = new int[NZ];
		RowIndex = new int[N + 1];

		for (int i = 0; i < NZ; i++) Value[i] = rand() % 10;
		for (int i = 0; i < NZ; i++) Col[i] = 1 + rand() % N;
		for (int i = 0; i < NZ; i++) RowIndex[i] = 1 + rand() % N;
	}

	void Multiplicate(vector<int> B)
	{
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < NZ; j++) if (RowIndex[j] == i) Value[j] *= B[i];
		}
	}

	void PrintValues() {
		for (int i = 0; i < NZ; i++) cout << Value[i] << " ";
		cout << endl;
	}
};

#include <iostream>
#include <vector>
using namespace std;


class triangularMatrix {
private:
	int size;
	int** Matrix;
public:
	triangularMatrix(int n) {
		size = n;

		Matrix = new int*[n*n];
		for (int k = 0; k < size; k++) {
			Matrix[k] = new int[size - k];
			for (int j = 0; j <= size - 1 - k; j++)
				Matrix[k][j] = rand() % 10;
		}
	}

	void print() {
		for (int k = 0; k < size; k++) {
			for (int j = 0; j < k; j++)
				cout << "\t";
			for (int j = 0; j <= size - 1 - k; j++)
				cout << Matrix[k][j] << "\t";
			cout << endl;
		}
	}

	int* operator[](size_t k) {
		if (0 <= k && k < size) {
			return Matrix[k];
		}
	}

	void MultVector(vector<int> B) {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) Matrix[i][j] *= B[i];
		}
	}


};